"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const constants_1 = require("./constants");
try { //Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch (error) {
    // console.log(`Error initializing app in routeEvents: ${ error} `);
}
//HACK: for now, if there are too many alerts, we delete some old alerts to 'make space'
// ...probably should figure out a better way to manage this... maybe it's fine?
//TODO: increase sice of maxNumAlerts in global constants (for testing keeping num small)
async function onCreateAlert(snap, context) {
    try {
        //if there are too many alerts, delete old alerts until there aren't too many
        //get ref to alert docs, ordered like this: [newest, new, ..., older, oldest]
        const alertsCollectionRef = admin.firestore()
            .collection(`USERS/${context.params.userId}/ALERTS/`).orderBy('date', 'desc');
        const alertsSnaps = await alertsCollectionRef.get();
        if (alertsSnaps.docs.length > constants_1.maxNumAlerts) {
            //delete the last maxNumAlerts - alertSnaps.docs.length, oldest alert docs
            for (let alertSnap of alertsSnaps.docs.slice(constants_1.maxNumAlerts)) {
                await alertSnap.ref.delete().catch(error => {
                    console.log('could not delete alert');
                });
            }
        }
    }
    catch (error) {
    }
    ;
}
exports.onCreateAlert = onCreateAlert;
exports.routeFunctions = {
    //onCreate: onCreate,
    // onDelete: onDeleteRouteDetails,
    onCreate: onCreateAlert,
};
//# sourceMappingURL=alertEvents.js.map