"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
//try catch because you can only initialize app once... acording to:
//https://codeburst.io/organizing-your-firebase-cloud-functions-67dc17b3b0da
try { //Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch (error) {
    //don't log the error, because EVERY file except index.html (I think it works there?) will have this error
    // console.log(`Error initializing app in routeEvents: ${ error} `);
}
/************************
Assumptions:
  -Every route has a grade
  -Every route has a wall angle
  -routes might not have a 'features' property
*/
async function onCreateUserSend(snap, context) {
    if (snap.exists) {
        //create or update a pointer to the user for future tasks
        //routefeedbackEvents.ts -> onDeleteRoute uses this path too!
        const routeToUserSendsDocPath = getRouteToUserSendsPath(context.params.gymId, context.params.routeId);
        const sendsDoc = admin.firestore().doc(routeToUserSendsDocPath);
        const sendsSnap = await sendsDoc.get();
        if (sendsSnap.exists) {
            //Does this user need to be added to the document?
            if (!sendsSnap.data().hasOwnProperty(context.params.userId)) {
                const userSend = { [context.params.userId]: true };
                await sendsSnap.ref.update(userSend);
            }
        }
        else { //the document didn't exist. Create the first send.
            const firstSend = { [context.params.userId]: true };
            await sendsSnap.ref.set(firstSend);
        }
        //create or update the analytics doc
        const sentRoute = await getSentRoute(context.params.gymId, context.params.mapsId, context.params.routeId);
        if (sentRoute) {
            //try to get the corresponding analytics doc
            const analyticsDoc = getAnalyticsDoc(context.params.userId);
            const analyticsSnap = await analyticsDoc.get();
            return await updateUserBoulderingAnalytics(sentRoute, analyticsSnap);
        }
        else {
            return null;
        }
    }
    else {
        return null;
    }
}
exports.onCreateUserSend = onCreateUserSend;
function getRouteToUserSendsPath(gymId, routeId) {
    if (gymId && routeId) {
        return `GYMS/${gymId}/ROUTESTOUSERSENDS/${routeId}`;
    }
    else {
        throw new Error('could not get path to SENDS: improper inputs');
    }
}
exports.getRouteToUserSendsPath = getRouteToUserSendsPath;
//sentRoute: the object representing the route document for the route that was just sent
//analyticsSnap: the snapshot of the /USERS/{userId}/ANALYTICS/{bouldering}/MONTHLY/{year-month}
async function updateUserBoulderingAnalytics(sentRoute, boulderingAnalyticsSnap) {
    console.log('updateUserBoulderingAnalytics');
    try {
        //create new analytics as blank/empty analytics object
        //we're going to update this object using the sentRoute information, then .set the analytics doc with thie object
        let newAnalytics = getEmptyBoulderingAnalytics();
        //if the analytics document already exists, update it
        if (boulderingAnalyticsSnap.exists) {
            newAnalytics = boulderingAnalyticsSnap.data();
        }
        //if the route has features set, use them to update the newAnalytics object
        if (sentRoute.hasOwnProperty('features') && Object.keys(sentRoute.features).length > 0) {
            //for each feature in the sent route, increment corresponding count in newAnalytics or create it with count = 1
            Object.keys(sentRoute.features).forEach(feature => {
                if (newAnalytics.features.hasOwnProperty(feature)) {
                    newAnalytics.features[feature] += 1;
                }
                else {
                    newAnalytics.features[feature] = 1;
                }
            });
        }
        else { //sent route had no features, so update the NA count
            newAnalytics['features']['NA'] += 1;
        }
        //increment or create grade property
        newAnalytics['grade'][sentRoute.grade] = newAnalytics['grade'].hasOwnProperty(sentRoute.grade) ?
            newAnalytics['grade'][sentRoute.grade] + 1 : 1;
        //increment or creat wallAngle property
        newAnalytics['wallAngle'][sentRoute.wallAngle] = newAnalytics['wallAngle'].hasOwnProperty(sentRoute.wallAngle) ?
            newAnalytics['wallAngle'][sentRoute.wallAngle] + 1 : 1;
        console.log('new analytics grade is:');
        console.log(newAnalytics.grade);
        console.log('about to .set(newAnalytics)');
        return await boulderingAnalyticsSnap.ref.set(newAnalytics);
    }
    catch (error) {
        throw error;
    }
    ;
}
//
//NOTE: if ./constants.ts -> UserBoulderingAnalytics interface changes, change this function accordingly
function getEmptyBoulderingAnalytics() {
    //use to initialize bouldering analytics objects
    console.log('.... getEmptyBoulderingAnalytics ....');
    return {
        // created: admin.firestore.FieldValue.serverTimestamp,
        grade: {},
        wallAngle: {},
        features: { NA: 0 }
    };
}
//update corresponding analytics document
//What is happening when this method is called? -> a user updates the send
//How can a user update a send?
//  -the 'numSends' is incremented
//  -the 'last' field has changed
//Need to fetch the corresponding route and update all of the relevant analytics properties
async function onUpdateSend(change, context) {
    //For now, the only way a sendDoc is updated is if the user sends a route again
    //if we add the ability to "undo" a send, then this function will have to account for that by updating the corresponding
    //analytics document and in the case where numsends becomes zero, delete the pointer from the corresponding route and
    //delete the send document (maybe should just directly do that from client)
    //FOR NOW: tell the user sending canno be 'undone'
    try {
        console.log('**********onUpdateSend triggered******');
        //get the corresponding route
        const sentRoute = await getSentRoute(context.params.gymId, context.params.mapsId, context.params.routeId);
        if (sentRoute) {
            //try to get the corresponding analytics doc
            const analyticsDoc = getAnalyticsDoc(context.params.userId);
            const analyticsSnap = await analyticsDoc.get();
            return await updateUserBoulderingAnalytics(sentRoute, analyticsSnap);
        }
        else {
            //can't retrieve the sent route, so we can't make any updates to the analytics doc
            return null;
        }
    }
    catch (error) {
        throw (error);
    }
    ;
}
//
async function getSentRoute(gymId, mapsId, routeId) {
    const sentRoutePath = `GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}`;
    const sentRouteDoc = admin.firestore().doc(sentRoutePath);
    const sentRouteSnap = await sentRouteDoc.get();
    let sentRoute = null;
    if (sentRouteSnap.exists) {
        return sentRouteSnap.data();
    }
    else {
        return null;
    }
}
function getAnalyticsDoc(userId) {
    //build the document name for this month's analytics document
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    const yearMonth = `${year}-${month}`;
    const userSendAnalyticsPath = `USERS/${userId}/ANALYTICS/bouldering/MONTHLY/${yearMonth}`;
    return admin.firestore().doc(userSendAnalyticsPath);
}
//NOTE: if a route is deleted, the cloud function onDeleteRoute will delete all of it's sends,
//currently, cannot distinguish between a send that we deleted by the user and a send that is deleted by cloud functions
//because the route was delted. If a user deletes a send, we want to update the analytics document for the user's sends
//if cloud functions delete the send because the route was deleted, we do NOT want to remove the send's influence on
//their analytics... SUGGESTION: users can not delete sends. We should warn them: when you say you sent, this can't be undone.
//
// export async function onDeleteSend(snap:functions.firestore.DocumentSnapshot,
// context:functions.EventContext) {
//   try{
//     if(snap.exists){
//       const userId = context.params.userId;
//       const gymId = context.params.gymId;
//       const mapsId = context.params.mapsId;
//       const routeId = context.params.routeId;
//
//       //Delete the corresponding SENDSBYROUTE/routeId document...
//       const userSendsPath  = `USERS/${userId}/SENDSBYGYM/${gymId}/SENDSBYMAPS/${mapsId}/SENDSBYROUTE/${routeId}`;
//       const userSendsRef = admin.firestore().doc(userSendsPath);
//
//       //don't let an error deleting break this method...
//       await userSendsRef.delete().catch();
//
//       //delete ROUTES/{routeId}/SENDS/sendsDoc if it exists
//       const routeToUserSendsDocPath = `GYMS/${context.params.gymId}
//       /MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}/SENDS/sendsDoc`;
//
//       //try to delete the send record from the corresponding route.
//       //this might be getting triggered from onDeleteRoute (it deletes all corresponding sends)
//       //...so see if the route still exists. if it does, try to delete this send from
//
//       //Might be faster to try to delete it and then catch any errors, rather than try to fetch it
//       const routeSendsDoc = admin.firestore().doc(routeToUserSendsDocPath);
//
//       await routeSendsDoc.delete().catch(); //fail gracefully and silently!
//
//       //TRY This if the above delete technique does not work...
//       // const routeSendsSnap = await routeSendsDoc.get();
//       //
//       // if(routeSendsSnap.exists){
//       //   await routeSendsSnap.ref.delete();
//       // }
//
//     }
//   }
//   catch(error){
//     throw error;
//   }
// }
exports.userSendsFunctions = {
    //onCreate: onCreate,
    // onDelete: onDeleteRouteDetails,
    onCreate: onCreateUserSend,
    onUpdate: onUpdateSend
};
//# sourceMappingURL=sendEvents.js.map