"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const routeEvents_1 = require("./routeEvents");
const routeCommentEvents_1 = require("./routeCommentEvents");
const feedbackEvents_1 = require("./feedbackEvents");
const sendEvents_1 = require("./sendEvents");
// import {routeDetailsFunctions} from './routeDetailsEvents';
const cronJobs_1 = require("./cronJobs");
const constants_1 = require("./constants");
/****** IMPORTANT IMPROVEMENTS: TODO... **********
[]whatever file creates a document should have an exported method, returning it's path
  (for example, sendEvents.ts creates the path `GYMS/${gymId}/ROUTESTOUSERSENDS/${routeId}`,
  when a new send is created, so, sendEvents.ts exports the method getRouteToUserSendsPath.
  This reduces spaggeti code. Need to change the path? Just change it in the path-genrating method);

*/
// import {Event} from 'firebase-functions';
try {
    admin.initializeApp();
}
catch (error) {
    // console.log(`Could not initialize app in index.ts: ${ error } `);
}
;
/*************** Cron ***********************/
//TODO: maybe move cron codes into relevant event file (Routes, Comments/Repies, etc.)
exports.everyOtherDayJob = functions.pubsub.topic('every-other-day')
    .onPublish(cronJobs_1.cronJobs.daily);
// exports.everyOtherDayJob = functions.pubsub
//   .topic('every-other-day')
//   .onPublish((message) => {
//     console.log("********firebase functions: every-other-day sub triggered!!! ********");
//     if (message.data) {
//       const dataString = Buffer.from(message.data, 'base64').toString();
//       console.log(`Message Data: ${dataString}`);
//     }
//
//     return true;
//   });
/*************** Routes **********************/
exports.onCreateRoute = functions.firestore.document(constants_1.routePath)
    .onCreate(routeEvents_1.routeFunctions.onCreate);
exports.onDeleteRoute = functions.firestore.document(constants_1.routePath)
    .onDelete(routeEvents_1.routeFunctions.onDeleteRoute);
/*************** Comments/Replies **********************/
exports.onCreateComment = functions.firestore.document(constants_1.routeCommentPath)
    .onCreate(routeCommentEvents_1.commentFunctions.onCreateComment);
exports.onDeleteComment = functions.firestore.document(constants_1.routeCommentPath)
    .onDelete(routeCommentEvents_1.commentFunctions.onDeleteComment);
exports.onCreateReply = functions.firestore.document(constants_1.routeCommentRepliesPath)
    .onCreate(routeCommentEvents_1.commentFunctions.onCreateReply);
exports.onDeleteReply = functions.firestore.document(constants_1.routeCommentRepliesPath)
    .onDelete(routeCommentEvents_1.commentFunctions.onDeleteReply);
/*********** Feedback ****************/
exports.onCreateFeedback = functions.firestore.document(constants_1.feedbackPath)
    .onCreate(feedbackEvents_1.feedbackFunctions.onCreate);
exports.onUpdateFeedback = functions.firestore.document(constants_1.feedbackPath)
    .onUpdate(feedbackEvents_1.feedbackFunctions.onUpdate);
/********* Sends *********************/
exports.onCreateUserSend = functions.firestore.document(constants_1.userSendsByRoutePath)
    .onCreate(sendEvents_1.userSendsFunctions.onCreate);
exports.onUpdateUserSend = functions.firestore.document(constants_1.userSendsByRoutePath)
    .onUpdate(sendEvents_1.userSendsFunctions.onUpdate);
/*********** Alerts ****************/
//# sourceMappingURL=index.js.map