"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
try { //Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch (error) {
    // console.log(`Error initializing app in routeEvents: ${ error} `);
}
//TODO: routeEvents onCreateRoute needs to be changed so that it creates the first Analytics document.
//TODO: need to test all functionality.
//NOTE: assumes that all fields in RouteFeedback are present
//code breaks if, for example, there is no rangeFilterVal.grade
//TODO: in the client, set all of the defaults to the current average?
async function onCreateFeedback(snap, context) {
    try {
        if (snap.exists) {
            const feedback = snap.data();
            //Two cases: 1.) Analytics doc does not exist yet. 2.) Analytics doc already exists.
            const pathToRouteDoc = `GYMS/${context.params.gymId}`
                + `/MAPS/${context.params.mapsId}`
                + `/ROUTES/${context.params.routeId}`;
            const analyticsDoc = admin.firestore()
                .doc(pathToRouteDoc + `/ANALYTICS/analyticsDoc`);
            const analyticsSnap = await analyticsDoc.get();
            //Case 1: Analytics doc does not exist yet
            if (!analyticsSnap.exists) {
                await analyticsSnap.ref.set(analyticsFromFeedback(feedback));
                return true;
            }
            else { //Case2: Analytics doc already exists
                const a = analyticsSnap.data(); //the old RouteAnalyticsData
                //this person has not given feedback on this route before (we're in the onCreate method, not onUpdate)
                //let O be old average, n be old total number of votes, and v be value of new vote
                //then the new average is O*n/(n+1) + v/(n+1) ...which is used below to avoid using looping to compute averages
                const newConsensusGrade = (a.consensusGrade * a.numGradeVotes / (a.numGradeVotes + 1))
                    + feedback.rangeFilterVal.grade / (a.numGradeVotes + 1);
                const newConsensusWallAngle = (a.consensusWallAngle * a.numWallAngleVotes) / (a.numWallAngleVotes + 1)
                    + feedback.rangeFilterVal.wallAngle / (a.numWallAngleVotes + 1);
                let newAttributeVotes = a.attributeVotes;
                Object.keys(feedback.attributes).forEach((attribute) => {
                    if (a.hasOwnProperty(attribute)) {
                        newAttributeVotes[attribute] = a[attribute] + 1;
                    }
                    else {
                        newAttributeVotes[attribute] = 1;
                    }
                });
                //update counters: numWallAngleVotes and numGradeVotes
                const newAnalticsData = {
                    consensusGrade: newConsensusGrade,
                    numGradeVotes: a.numGradeVotes + 1,
                    consensusWallAngle: newConsensusWallAngle,
                    numWallAngleVotes: a.numWallAngleVotes + 1,
                    attributeVotes: newAttributeVotes
                };
                await analyticsSnap.ref.set(newAnalticsData);
                return true;
            }
        }
        else { //the snapshot didn't exist so we can't update anything
            return false;
        }
    }
    catch (error) {
        throw error;
    }
    ;
}
exports.onCreateFeedback = onCreateFeedback;
async function onUpdateFeedback(change, context) {
    try {
        if (change.before.exists && change.after.exists) {
            //this is onUpdate, so the analytics page should already exist. If it
            //doesn't, just throw an error
            const previousFeedback = change.before.data();
            const afterFeedback = change.after.data();
            //Two cases: 1.) Analytics doc does not exist yet. 2.) Analytics doc already exists.
            const pathToRouteDoc = `GYMS/${context.params.gymId}`
                + `/MAPS/${context.params.mapsId}`
                + `/ROUTES/${context.params.routeId}`;
            const analyticsDoc = admin.firestore()
                .doc(pathToRouteDoc + `/ANALYTICS/analyticsDoc`);
            const analyticsSnap = await analyticsDoc.get();
            //Case 1: Analytics doc does not exist...but this is onUpdate, so it should exist already. Throw an error.
            if (!analyticsSnap.exists) {
                throw new Error('feedbackEvents, onUpdateFeedback: ERROR. Can not update analytics doc.');
            }
            else {
                //Case 2: Analytics doc already existed
                const a = analyticsSnap.data(); //the old RouteAnalyticsData
                //this person has not given feedback on this route before (we're in the onCreate method, not onUpdate)
                //let O be old average, n be old total number of votes, and v be value of new vote
                //then the new average is O*n/(n+1) + v/(n+1) ...which is used below to avoid using looping to compute averages
                const newConsensusGrade = (a.consensusGrade * a.numGradeVotes - previousFeedback.rangeFilterVal.grade + afterFeedback.rangeFilterVal.grade) /
                    (a.numGradeVotes);
                const newConsensusWallAngle = (a.consensusWallAngle * a.numWallAngleVotes - previousFeedback.rangeFilterVal.wallAngle + afterFeedback.rangeFilterVal.wallAngle) /
                    (a.numGradeVotes);
                let newAttributeVotes = a.attributeVotes;
                Object.keys(afterFeedback.attributes).forEach((attribute) => {
                    if (a.hasOwnProperty(attribute)) {
                        //add one if this user did not vote for this attribute before
                        if (!previousFeedback.attributes.hasOwnProperty(attribute)) {
                            newAttributeVotes[attribute] = newAttributeVotes[attribute] + 1;
                        }
                        //no need for else: already voded for this, so don't update the count
                    }
                    else { //analytics does not have the attribute, so creat it
                        newAttributeVotes[attribute] = 1;
                    }
                });
                //update counters: numWallAngleVotes and numGradeVotes
                const newAnalticsData = {
                    consensusGrade: newConsensusGrade,
                    numGradeVotes: a.numGradeVotes,
                    consensusWallAngle: newConsensusWallAngle,
                    numWallAngleVotes: a.numWallAngleVotes,
                    attributeVotes: newAttributeVotes
                };
                await analyticsSnap.ref.set(newAnalticsData);
                return true;
            }
        }
        else {
            throw new Error('feedbackEvents, onUpdateFeedback: ERROR... change.before or change.after DNE');
        }
    }
    catch (error) {
        throw error;
    }
    ;
}
exports.onUpdateFeedback = onUpdateFeedback;
//takes a single feedback object and creates an analytics object representing
//analytics based off of the single piece of feedback
function analyticsFromFeedback(feedback) {
    let attributeVotes = {};
    Object.keys(feedback.attributes).forEach((key) => attributeVotes[key] = 1);
    return {
        consensusGrade: feedback.rangeFilterVal.grade,
        numGradeVotes: 1,
        consensusWallAngle: feedback.rangeFilterVal.wallAngle,
        numWallAngleVotes: 1,
        attributeVotes: attributeVotes
    };
}
exports.feedbackFunctions = {
    //onCreate: onCreate,
    // onDelete: onDeleteRouteDetails,
    onCreate: onCreateFeedback,
    onUpdate: onUpdateFeedback
};
//# sourceMappingURL=feedbackEvents.js.map