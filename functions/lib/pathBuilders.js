"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/******** ROUTES *********/
//omit routeId for a path to the specified ROUTES collection
//include routeId for path to a specific route doc
function getRoutePath(gymId, mapsId, routeId = null) {
    if (routeId) { //return path to specific document
        return `GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}`;
    }
    else { //return path to the specified ROUTES collection
        return `GYMS/${gymId}/MAPS/${mapsId}/ROUTES`;
    }
}
exports.getRoutePath = getRoutePath;
//--> FEEDBACK
function getFeedbackPath(gymId, mapsId, routeId, authorId = null) {
    const routePath = getRoutePath(gymId, mapsId, routeId);
    if (authorId) { //if authorId is provided, return path to a specific FEEDBACK document
        return routePath + `/FEEDBACK/${authorId}`;
    }
    else {
        return routePath + "/FEEDBACK";
    }
}
exports.getFeedbackPath = getFeedbackPath;
function getUserPath(userId) {
    if (userId && typeof userId === typeof "string") {
        return `USERS/${userId}`;
    }
    else {
        throw new Error("pathBuilders.ts, getUserPath ERROR: userId was falsey or not a string");
    }
}
exports.getUserPath = getUserPath;
//# sourceMappingURL=pathBuilders.js.map