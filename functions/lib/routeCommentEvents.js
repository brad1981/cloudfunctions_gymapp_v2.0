"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const constants_1 = require("./constants");
try { //Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch (error) {
    // console.log(`Error initializing app in routeEvents: ${ error} `);
}
//create an alert for the person whose route was commented on
//the alert's id is the commentId
//update the numComments counter under the corresponding route document
//TODO: onCreateAlert: manage too many alerts situation. Maybe simply enforce max num alerts (10?)
//TODO: Deal with possibiltiy of acruing too many comments: if a route gets hundreds of thousands of comments (more?)
//...when the route is deleted, cleaning up those comment documents could take too long or otherwise cause problems
//...maybe just start deleting old comments when a route gets more than 500 (or whatever) comments
async function onCreateComment(snap, context) {
    try {
        if (snap.exists) {
            const commentData = snap.data();
            const parentRouteRef = admin.firestore()
                .doc(`GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}`);
            const parentRouteSnap = await parentRouteRef.get();
            const parentRouteData = parentRouteSnap.data();
            let numComments = 0;
            if (parentRouteData.hasOwnProperty('numComments')) {
                numComments = parentRouteData.numComments + 1;
            }
            else {
                numComments = 1;
            }
            await parentRouteRef.update({ 'numComments': numComments });
            //TODO: ensure that routeData.authorId !== commentData.authorId (if equal, do not alert!)
            if (commentData.hasOwnProperty('authorId')) {
                //the parent collection (COMMENTS) of this comment is embedded in a route document...
                //...that route document was created by some user
                //Add an alert to that user's ALERTS collection
                //use this comment's id as the id for the alert document
                const newAlertsDoc = admin.firestore()
                    .doc(`USERS/${parentRouteData.authorId}/ALERTS/${snap.id}`);
                await newAlertsDoc.set({ type: 'routeComment',
                    date: commentData.date,
                    pathToRoute: `GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}`,
                    authorMug: commentData.authorMug
                    //TODO: maybe include text preview of comment
                    //TODO: maybe include userName and photoURL of author
                });
            }
        }
        else {
            return null;
        }
        //update numComments for this route
        //TODO:
        //alert user that someone commented on their route
    }
    catch (error) {
    }
    ;
}
exports.onCreateComment = onCreateComment;
//if the comment has replies, delete all of the comment's replies
//update parent route's numComments property
//TODO: try-catch to handle errors
async function onDeleteComment(snap, context) {
    try {
        if (snap.exists) {
            // console.log('snapExists...');
            const deletedComment = snap.data();
            console.log(deletedComment);
            const commentId = context.params.commentId;
            const parentRouteRef = admin.firestore()
                .doc(`GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}`);
            const parentRouteSnap = await parentRouteRef.get();
            //parent route's deletion might have triggered onDeleteComment. If so, do not update its numComments field
            if (parentRouteSnap.exists) {
                const parentRouteData = parentRouteSnap.data();
                let numComments = 0;
                if (parentRouteData.hasOwnProperty('numComments')) {
                    numComments = parentRouteData.numComments - 1;
                }
                //if there are still comments left, update the count
                if (numComments > 0) {
                    await parentRouteRef.update({ 'numComments': numComments });
                }
                else {
                    //there are no more comments, so delete the counter
                    // FirebaseFirestore.FieldValue.delete() //<-- used to delete numComments with this, but now it's deprecated?
                    await parentRouteRef.update({ 'numComments': 0 });
                }
            }
            //Delete replies subcollection if it exists
            //retrieve all repliy docs
            const repliesCollectionReference = admin.firestore()
                .collection(`GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}/COMMENTS/${commentId}/REPLIES/`);
            const replySnaps = await repliesCollectionReference.get();
            console.log('got ', replySnaps.docs.length, ' reply snaps');
            //if this comment has replies in a REPLIES subcollection, delete them all
            if (replySnaps.docs.length > 0) {
                replySnaps.docs.forEach(async (replySnap) => {
                    // console.log('Deleting reply with text: ', replySnap.data().comment);
                    await replySnap.ref.delete();
                });
            }
            return true;
        }
        //console.log('comment snap did not exist')
        return true;
    }
    catch (error) {
        console.log(error);
        throw error;
    }
    ;
}
exports.onDeleteComment = onDeleteComment;
async function onCreateReply(snap, context) {
    try {
        if (snap.exists) {
            const replyData = snap.data();
            const parentCommentRef = admin.firestore()
                .doc(`GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}/COMMENTS/${context.params.commentId}`);
            const parentCommentSnap = await parentCommentRef.get();
            const parentCommentData = parentCommentSnap.data();
            let numReplies = 0;
            if (parentCommentData.hasOwnProperty('numReplies')) {
                numReplies = parentCommentData.numReplies + 1;
            }
            else {
                numReplies = 1;
            }
            await parentCommentRef.update({ 'numReplies': numReplies });
            if (replyData.hasOwnProperty('authorId')) {
                //the parent collection (REPLIES) of this reply is embedded in a comment document
                //...that comment document was created by some user
                //Add an alert to that user's ALERTS collection
                //use this reply's id as the id for that alert document
                const newAlertsDoc = admin.firestore()
                    .doc(`USERS/${parentCommentData.authorId}/ALERTS/${snap.id}`);
                await newAlertsDoc.set({ type: 'routeCommentReply',
                    date: replyData.date,
                    pathToComment: `GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}/COMMENTS/${context.params.commentId}`,
                    authorMug: replyData.authorMug
                    //TODO: maybe include text preview of comment
                    //TODO: maybe include userName and photoURL of author
                });
            }
        }
    }
    catch (error) {
        //handle error
    }
    ;
}
exports.onCreateReply = onCreateReply;
//update parent comment's num comments count
async function onDeleteReply(snap, context) {
    try {
        if (snap.exists) {
            const parentCommentRef = admin.firestore()
                .doc(`GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}/COMMENTS/${context.params.commentId}`);
            const parentCommentSnap = await parentCommentRef.get();
            if (parentCommentSnap.exists) {
                const parentCommentData = parentCommentSnap.data();
                let numReplies = 0;
                if (parentCommentData.hasOwnProperty('numReplies')) {
                    numReplies = parentCommentData.numReplies - 1;
                }
                numReplies = numReplies >= 0 ? numReplies : 0;
                await parentCommentRef.update({ 'numReplies': numReplies });
            }
        }
    }
    catch (error) {
    }
    ;
}
exports.onDeleteReply = onDeleteReply;
async function deleteAllCommentsAndReplies(gymId, mapsId, routeId) {
    try {
        const COMMENTSpath = getPathToCOMMENTS(gymId, mapsId, routeId);
        const commentsCollectionReference = admin.firestore()
            .collection(COMMENTSpath);
        const commentsSnaps = await commentsCollectionReference.get();
        //if this route has comments
        if (commentsSnaps.docs.length > 0) {
            for (let commentSnap of commentsSnaps.docs) {
                //if this comment has replies, delete its replies
                if (commentSnap.exists) {
                    const repliesCollectionReference = admin.firestore()
                        .collection(getPathToREPLIES(gymId, mapsId, routeId, commentSnap.id));
                    const repliesSnaps = await repliesCollectionReference.get();
                    if (repliesSnaps.docs.length > 0) {
                        for (let replySnap of repliesSnaps.docs) {
                            if (replySnap.exists) {
                                await replySnap.ref.delete();
                            }
                        }
                    }
                    //delete the comment document
                    await commentSnap.ref.delete();
                }
            }
        }
    }
    catch (error) {
        console.log(error);
        throw error;
    }
}
exports.deleteAllCommentsAndReplies = deleteAllCommentsAndReplies;
//get data from ROUTES document. if includeRef == true, return the corresponding document reference (for further use)
async function getRouteData(gymId, mapsId, routeId, includeRef = false) {
    try {
        const routePath = getPathToRouteDoc(gymId, mapsId, routeId);
        const routeDataAndMaybeRef = await constants_1.getDocDataAtPath(routePath, includeRef);
        if (includeRef) {
            return {
                route: routeDataAndMaybeRef.data,
                ref: routeDataAndMaybeRef.ref
            };
        }
        else {
            return routeDataAndMaybeRef;
        }
    }
    catch (error) {
        throw error;
    }
    ;
}
function getPathToCOMMENTS(gymId, mapsId, routeId) {
    if (gymId && mapsId && routeId) {
        return `GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}/COMMENTS/`;
    }
    else {
        throw new Error('could not get path to COMMENTS: improper inputs');
    }
}
function getPathToREPLIES(gymId, mapsId, routeId, commentId) {
    if (commentId) {
        try {
            return getPathToCOMMENTS(gymId, mapsId, routeId) + `${commentId}/REPLIES/`;
        }
        catch (error) {
            throw error;
        }
        ;
    }
    else {
        throw new Error('could not get path to REPLIES: improper input');
    }
}
function getPathToComment(gymId, mapsId, routeId, commentId) {
    return `GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}/COMMENTS/${commentId}`;
}
//get the path to the ROUTE document
function getPathToRouteDoc(gymId, mapsId, routeId) {
    return `GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}`;
}
exports.commentFunctions = {
    onCreateComment: onCreateComment,
    onDeleteComment: onDeleteComment,
    onCreateReply: onCreateReply,
    onDeleteReply: onDeleteReply
};
//# sourceMappingURL=routeCommentEvents.js.map