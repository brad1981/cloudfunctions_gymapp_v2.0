"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/********* PATHS ******/
exports.bucketPath = 'gymionic2.appspot.com/';
exports.routeDetailPath = `GYMS/{gymId}/MAPS/{mapsId}/ROUTES/{routeId}/DETAILS/detailsDoc`;
exports.routePath = `GYMS/{gymId}/MAPS/{mapsId}/ROUTES/{routeId}`;
exports.routeCommentPath = `GYMS/{gymId}/MAPS/{mapsId}/ROUTES/{routeId}/COMMENTS/{commentId}`;
exports.routeCommentRepliesPath = exports.routeCommentPath + `/REPLIES/{replyId}`;
exports.feedbackPath = exports.routePath + `/FEEDBACK/{uid}`;
exports.userSendsByRoutePath = `USERS/{userId}/SENDSBYGYM/{gymId}/SENDSBYMAPS/{mapsId}/SENDSBYROUTE/{routeId}`;
exports.routeAnalyticsPath = exports.routePath + `/ANALYTICS/analytics`; //aggregate suggested grades and features here
exports.alertPath = 'USERS/{userId}/ALERTS/{alertId}';
// import * as functions from 'firebase-functions'; //use to monitor EVENTS (onWrite, onChange, onDelete, etc.)
const admin = require("firebase-admin");
try { //Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch (error) {
    // console.log(`Error initializing app in routeEvents: ${ error} `);
}
/********* helper functions *********/
//supply path to desired document. if includeRef == true, DocumentReference to the document will be returned as well
async function getDocDataAtPath(path, includeRef = false) {
    try {
        const ref = admin.firestore().doc(path);
        const snap = await ref.get();
        const data = snap.data();
        if (data.keys().length < 1) {
            return null;
        }
        if (includeRef) {
            return {
                data: data,
                ref: ref
            };
        }
        else {
            return ref;
        }
    }
    catch (error) {
        throw error;
    }
    ;
}
exports.getDocDataAtPath = getDocDataAtPath;
/*************Path Helpers********************/
function getPathToGymsCollection() {
    return "/GYMS/";
}
exports.getPathToGymsCollection = getPathToGymsCollection;
/************ Magic Numbers **************/
exports.maxNumAlerts = 4;
//# sourceMappingURL=constants.js.map