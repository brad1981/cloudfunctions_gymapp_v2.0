"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
try { //Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch (error) {
    // console.log(`Error initializing app in routeEvents: ${ error} `);
}
/******************** Route-related Cron Jobs ***************************/
//
// exports.everyOtherDayJob = functions.pubsub
//   .topic('every-other-day')
//   .onPublish((message) => {
//     console.log("********firebase functions: every-other-day sub triggered!!! ********");
//     if (message.data) {
//       const dataString = Buffer.from(message.data, 'base64').toString();
//       console.log(`Message Data: ${dataString}`);
//     }
//
//     return true;
//   });
//CAUTION...TODO: Cron behavior is currently not scalable.
//Probably should create a seperate pub event (through google app engine's pub/sub cron services)
//...for each time zone (on earth? How to manage adding time zones?)
//Then, there will be seperate callbacks subscribing to each of those time zone events so that no single callback
//is ever working on the entire earth's data. There's probably a better way to do this, but that will prob work?
const dailyCallback = async (message) => {
    // console.log("********firebase functions: every-other-day sub triggered!!! ********");
    if (message.data) {
        const dataString = Buffer.from(message.data, 'base64').toString();
        console.log(`Message Data: ${dataString}`);
    }
    //get array of all gyms
    const gymDocs = admin.firestore().collection("/GYMS/");
    const gymRefs = await gymDocs.listDocuments();
    const now = admin.firestore.Timestamp.now();
    const oneDayAgoMilis = now.toMillis() - 86400000; //now - 24 hours worth of miliseconds
    const oneDayAgoTimestamp = admin.firestore.Timestamp.fromMillis(oneDayAgoMilis);
    gymRefs.forEach(async (gymRef) => {
        // console.log('CRON JOB, got gymRef for gym with id: ', gymRef.id);
        const mapsRef = gymRef.collection("MAPS").where("test", "==", true);
        //collect active map ids
        const mapSnaps = await mapsRef.get();
        mapSnaps.forEach(async (mapSnap) => {
            const routeColRef = admin.firestore()
                .collection(`/GYMS/${gymRef.id}/MAPS/${mapSnap.id}/ROUTES/`);
            const routeSnaps = await routeColRef.where("firstDeleteDate", "<", oneDayAgoTimestamp).get();
            routeSnaps.forEach(async (routeSnap) => {
                // console.log('dailyCron, got expired route with id: ');
                console.log(routeSnap.id);
                const routeData = routeSnap.data();
                if (routeData.hasOwnProperty('deleteVoterIds')) {
                    if (routeData.deleteVoterIds.length > 0) {
                        //if fewer contests than deleteVotes, delete the route
                        if (routeData.hasOwnProperty('contestVoterIds')) {
                            if (routeData.contestVoterIds.length < routeData.deleteVoterIds.length) {
                                //TESTED: pass
                                // console.log("dailyCron, fewer contest than deleteVotes, DELETING NOW" );
                                return routeSnap.ref.delete();
                            }
                            else { //there is a tie or there are more contests than deletes
                                //reset the voting
                                // console.log("dailyCron, more contest than deleteVotes (or tie), reseting votes" );
                                //TESTED: pass
                                return resetVotes(routeSnap.ref);
                            }
                        }
                        else { //the deletes are uncontested, so delete now
                            // console.log("dailyCron, deleteVotes are uncontested , deleting route" );
                            //TESTED: pass
                            return routeSnap.ref.delete();
                        }
                    }
                    else { //there are no deleteVoterIds because the array was empty (but present)
                        //remove deleteVoterIds, contestVoterIds and firstDeleteDate field
                        // console.log("there were no deleteVotes: empy array, reseting votes" );
                        return resetVotes(routeSnap.ref);
                    }
                }
                else {
                    //else: there was a firstDeleteDate, but for some reason, no deleteVoterIds field
                    // console.log("there was a firstDeleteDate, but for some reason, no deleteVoterIds field" );
                    return resetVotes(routeSnap.ref);
                }
            });
        });
    });
    return true;
};
async function resetVotes(routeRef) {
    let updateObject = {
        contestVoterIds: admin.firestore.FieldValue.delete(),
        deleteVoterIds: admin.firestore.FieldValue.delete(),
        firstDeleteDate: admin.firestore.FieldValue.delete()
    };
    return routeRef.update(updateObject);
}
/***************** END Route-related Cron Jobs **************************/
exports.cronJobs = {
    daily: dailyCallback
};
//# sourceMappingURL=cronJobs.js.map