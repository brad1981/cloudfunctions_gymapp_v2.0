/********* PATHS ******/
export const bucketPath = 'gymionic2.appspot.com/';

export const routeDetailPath = `GYMS/{gymId}/MAPS/{mapsId}/ROUTES/{routeId}/DETAILS/detailsDoc`;
export const routePath = `GYMS/{gymId}/MAPS/{mapsId}/ROUTES/{routeId}`;
export const routeCommentPath = `GYMS/{gymId}/MAPS/{mapsId}/ROUTES/{routeId}/COMMENTS/{commentId}`
export const routeCommentRepliesPath = routeCommentPath + `/REPLIES/{replyId}`;
export const feedbackPath = routePath +  `/FEEDBACK/{uid}`;
export const userSendsByRoutePath = `USERS/{userId}/SENDSBYGYM/{gymId}/SENDSBYMAPS/{mapsId}/SENDSBYROUTE/{routeId}`;
export const routeAnalyticsPath = routePath +  `/ANALYTICS/analytics`; //aggregate suggested grades and features here


export const alertPath = 'USERS/{userId}/ALERTS/{alertId}';

// import * as functions from 'firebase-functions'; //use to monitor EVENTS (onWrite, onChange, onDelete, etc.)
import * as admin from 'firebase-admin';

try{//Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch(error){
  // console.log(`Error initializing app in routeEvents: ${ error} `);
}
/********* helper functions *********/
//supply path to desired document. if includeRef == true, DocumentReference to the document will be returned as well
export async function getDocDataAtPath(path:string, includeRef:boolean = false):
Promise< any| {data:any, ref: FirebaseFirestore.DocumentReference}>{
  try{
    const ref = admin.firestore().doc(path);
    const snap = await ref.get();
    const data = snap.data();
    if (data.keys().length < 1){
      return null;
    }
    if(includeRef){
      return {
        data:data,
        ref:ref
      };
    }else{
      return ref;
    }
  }
  catch(error){
    throw error;
  };
}

/***** INTERFACES ******/
export interface User {
  displayName:string;
  photoURL:string;
}
export interface HoldObject {
  color: number;
  pointCor: {x: number, y:number};
  radius: number;
}

export interface RouteObject {
  pushKey:string; //for finding the comment and updating or removing it
  topo: {photos: string[], holds: HoldObject[]};
  name: string;
  authorName:string; //TODO: phase in userName for storage location rather than authorId
  authorMug?:string; //storage url (also used for local storage)
  authorId?: string;
  features: any;    //TODO implement interface for feature
  location: any; // TODO: import location object from feed.ts to this file.
  mapId: string;
  floor?: number;
  imageUrl?:string;
  coverPhoto?:string;//path to image in firebase storage
  betaPhotos?:string[]; // TODO determine type
  grade: number;
  consGrade: number;
  numComments?:number;
  consensusGrade?:number;
  wallAngle?:number;

  deleteVoterIds?:string[];
  firstDeleteDate?:admin.firestore.Timestamp;
  contestVoterIds?:string[];
}

export interface RouteFeedback{
  uid:string
  attributes: {[key:string]:boolean}
  rangeFilterVal:{
    grade:number,
    wallAngle:number
  }
}

export interface Send{
  first: any, //actually a firebase.firestore.FieldValue.serverTimestamp() instance, but can't find type
  last: any; //actually a firebase.firestore.FieldValue.serverTimestamp() instance, but can't find type


  // timesToTries:{
  //   [key:number]:number; //key: the timestamp of the send. value: number of tries needed to send
  // };
  numSends: number;
  grade: number;

  setBy: string; //for querying official vs diy

  floor: number;
  color?: string;

  pushKey?:string;
}

export interface RouteAnalyticsData{
  consensusGrade?:number;
  numGradeVotes: number;
  consensusWallAngle:number;
  numWallAngleVotes?: number;
  numSends?:number;

  attributeVotes:{
    [key:string]:number
  }
}

export interface UserBoulderingAnalytics{
  //when building 'created', use admin.firestore.FieldValue.serverTimestamp
  created?: ()=> admin.firestore.FieldValue ; //...FieldValue.serverTimestamp is a callback that returns a FieldValue
  grade:{[key:number]:number};//{'grade' : 'number of times sent routes with that grade'}
  wallAngle: {[key:number]:number}; //{'angle-index':'number of times sent routes with that that angle-index'}
  features: {[key:string]:number;//{'feature string': 'number of times sent routes with that feature string'}
              NA:number //for tracking how many routes did not have any features information
            };
}

export interface MyRoutes{
  [key:string]:{//gym id
    [key:string]:string[] //mapId : [routeId, routeId...]
  }
}

//This is basically a Reply interface from the route-details provider
export interface RouteCommentReply{
  date: Date;
  authorId: string;
  authorMug?:string;
  comment: string;
}

//This is basically a Comment interface from the route-details provider
export interface RouteComment{
  date: Date;
  authorId:string;
  authorMug?: string; //url to the author's avatar image
  comment: string;
  numReplies?:number;

}

/*************Path Helpers********************/
export function getPathToGymsCollection():string{
  return "/GYMS/";
}

/************ Magic Numbers **************/
export const maxNumAlerts = 4 ;
