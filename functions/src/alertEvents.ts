import * as functions from 'firebase-functions'; //use to monitor EVENTS (onWrite, onChange, onDelete, etc.)
import * as admin from 'firebase-admin';

import {maxNumAlerts } from './constants';

try{//Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch(error){
  // console.log(`Error initializing app in routeEvents: ${ error} `);
}

//HACK: for now, if there are too many alerts, we delete some old alerts to 'make space'
// ...probably should figure out a better way to manage this... maybe it's fine?
//TODO: increase sice of maxNumAlerts in global constants (for testing keeping num small)
export async function onCreateAlert(snap: functions.firestore.DocumentSnapshot,
  context: functions.EventContext){
    try{
      //if there are too many alerts, delete old alerts until there aren't too many
      //get ref to alert docs, ordered like this: [newest, new, ..., older, oldest]
      const alertsCollectionRef = admin.firestore()
            .collection(`USERS/${context.params.userId}/ALERTS/`).orderBy('date','desc');
      const alertsSnaps = await alertsCollectionRef.get();
      if(alertsSnaps.docs.length > maxNumAlerts){
        //delete the last maxNumAlerts - alertSnaps.docs.length, oldest alert docs
        for(let alertSnap of alertsSnaps.docs.slice(maxNumAlerts)){
          await alertSnap.ref.delete().catch(error=>{
            console.log('could not delete alert');
          });
        }
      }
    }
    catch(error){
    };

  }

export const routeFunctions = {
  //onCreate: onCreate,
  // onDelete: onDeleteRouteDetails,
  onCreate: onCreateAlert,

};
