/******** ROUTES *********/
//omit routeId for a path to the specified ROUTES collection
//include routeId for path to a specific route doc
export function getRoutePath(gymId:string, mapsId:string, routeId:string = null){
  if(routeId){//return path to specific document
    return  `GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}`;
  }else{//return path to the specified ROUTES collection
    return `GYMS/${gymId}/MAPS/${mapsId}/ROUTES`;
  }
}

//--> FEEDBACK
export function getFeedbackPath(gymId:string, mapsId:string, routeId:string, authorId:string=null){
  const routePath = getRoutePath(gymId,mapsId,routeId);
  if(authorId){//if authorId is provided, return path to a specific FEEDBACK document
    return routePath+`/FEEDBACK/${authorId}`;
  }else{
    return  routePath + "/FEEDBACK";
  }
}

export function getUserPath(userId:string){
  if(userId && typeof userId === typeof "string"){
    return `USERS/${userId}`
  }else{
    throw new Error("pathBuilders.ts, getUserPath ERROR: userId was falsey or not a string")
  }
}
