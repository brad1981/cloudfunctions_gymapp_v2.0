import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import {routeFunctions} from './routeEvents';
import { commentFunctions} from './routeCommentEvents';
import { feedbackFunctions } from './feedbackEvents';
import { userSendsFunctions } from './sendEvents';
// import {routeDetailsFunctions} from './routeDetailsEvents';
import { cronJobs } from './cronJobs';

import {routePath, routeCommentPath,
  feedbackPath, routeCommentRepliesPath, userSendsByRoutePath } from './constants';

/****** IMPORTANT IMPROVEMENTS: TODO... **********
[]whatever file creates a document should have an exported method, returning it's path
  (for example, sendEvents.ts creates the path `GYMS/${gymId}/ROUTESTOUSERSENDS/${routeId}`,
  when a new send is created, so, sendEvents.ts exports the method getRouteToUserSendsPath.
  This reduces spaggeti code. Need to change the path? Just change it in the path-genrating method);

*/


// import {Event} from 'firebase-functions';
try{
  admin.initializeApp();
}
catch(error){
  // console.log(`Could not initialize app in index.ts: ${ error } `);
};

/*************** Cron ***********************/
//TODO: maybe move cron codes into relevant event file (Routes, Comments/Repies, etc.)
exports.everyOtherDayJob = functions.pubsub.topic('every-other-day')
.onPublish(cronJobs.daily);

// exports.everyOtherDayJob = functions.pubsub
//   .topic('every-other-day')
//   .onPublish((message) => {
//     console.log("********firebase functions: every-other-day sub triggered!!! ********");
//     if (message.data) {
//       const dataString = Buffer.from(message.data, 'base64').toString();
//       console.log(`Message Data: ${dataString}`);
//     }
//
//     return true;
//   });


/*************** Routes **********************/
exports.onCreateRoute = functions.firestore.document(routePath)
       .onCreate(routeFunctions.onCreate);

exports.onDeleteRoute= functions.firestore.document(routePath)
       .onDelete(routeFunctions.onDeleteRoute);



/*************** Comments/Replies **********************/
exports.onCreateComment = functions.firestore.document(routeCommentPath)
       .onCreate(commentFunctions.onCreateComment);

exports.onDeleteComment = functions.firestore.document(routeCommentPath)
       .onDelete(commentFunctions.onDeleteComment);

exports.onCreateReply = functions.firestore.document(routeCommentRepliesPath)
       .onCreate(commentFunctions.onCreateReply);

exports.onDeleteReply = functions.firestore.document(routeCommentRepliesPath)
       .onDelete(commentFunctions.onDeleteReply);


/*********** Feedback ****************/
exports.onCreateFeedback = functions.firestore.document(feedbackPath)
       .onCreate(feedbackFunctions.onCreate);
exports.onUpdateFeedback = functions.firestore.document(feedbackPath)
       .onUpdate(feedbackFunctions.onUpdate);

/********* Sends *********************/

exports.onCreateUserSend = functions.firestore.document(userSendsByRoutePath)
       .onCreate(userSendsFunctions.onCreate);

exports.onUpdateUserSend = functions.firestore.document(userSendsByRoutePath)
       .onUpdate(userSendsFunctions.onUpdate);


/*********** Alerts ****************/
