import * as functions from 'firebase-functions'; //use to monitor EVENTS (onWrite, onChange, onDelete, etc.)
import * as admin from 'firebase-admin';
//import firebase from '@firebase/app';
//import '@firebase/firestore';

//try catch because you can only initialize app once... acording to:
//https://codeburst.io/organizing-your-firebase-cloud-functions-67dc17b3b0da
import { bucketPath,
  RouteObject,
  // MyRoutes,
  RouteFeedback,
  // RouteAnalyticsData,
  User} from './constants';

import { deleteAllCommentsAndReplies } from './routeCommentEvents';
import {getRouteToUserSendsPath} from './sendEvents';
import { getUserPath } from './pathBuilders'

try{//Even though initilized in index.ts, you HAVE to do this here too
    admin.initializeApp();
}
catch(error){
  // console.log(`Error initializing app in routeEvents: ${ error} `);
}

/*
// CODE-ANNOTATION: What I learned working on this (time-saving tips)

NOTE: Read through this (follow link). Details important changes to cloud functions API
https://firebase.google.com/docs/functions/firestore-events
*/

/* onCreateRoute responsibilities: (check boxes with '*' after rules are uploaded and tested)
 []create or update `USERS/${routeData.authorId}/LISTS/myRoutes` with
   new route's id (see global constants interface: MyRoutes )
 []sanitize content (title and route description)

*/

export async function onCreateRoute(snap:functions.firestore.DocumentSnapshot,
// context:functions.EventContext
){
  if(snap.exists){

    const routeData = snap.data() as RouteObject;

    if(routeData.hasOwnProperty('authorId')){
      //********** FEEDBACK/{uid} ********//
      //create feedback doc for this route (the creator submitted the first feedback)
      const feedback:RouteFeedback  = {
        uid: routeData.authorId,
        rangeFilterVal: {
          grade: routeData.grade,
          wallAngle: routeData.wallAngle
        },
        attributes: routeData.features
      };

      const feedbackPath = snap.ref.path + `/FEEDBACK/${routeData.authorId}`;
      const feedbackDoc = admin.firestore().doc(feedbackPath);
      await feedbackDoc.set(feedback);
      //NOTE: cloud function feedbackEvents.onCreateFeedback will generate the analytics document

      //update this routeData document with the author's photoURL
      const authorId = routeData.authorId;
      const authorUserPath = getUserPath(authorId);
      const userDocRef = admin.firestore().doc(authorUserPath);
      const userDocSnap = await userDocRef.get();
      const user = userDocSnap.data() as User;
      console.log(`routeEvents.ts, onCreateRoute, user.photoURL is: ${user.photoURL}` )

      admin.firestore().doc(snap.ref.path).update({authorMug:user.photoURL}).catch(error=>{
        console.log("ERROR: could not update");
        console.log(error);
      });


      // const myRoutesDoc = admin.firestore()
      // .doc(`USERS/${routeData.authorId}/LISTS/myRoutes`);
      //
      // //get the myRoutes data:
      // const myRoutesSnap  = await myRoutesDoc.get();
      // let myRoutes: MyRoutes = {};
      // let routesList: string[] = [];
      //
      // //if myROutes data has already been initialized with other routeId's for this gymId and this mapsId...
      // if( myRoutesSnap.exists ){
      //   myRoutes= myRoutesSnap.data();
      //   if(myRoutes.hasOwnProperty(context.params.gymId)
      //   && myRoutes[context.params.gymId].hasOwnProperty(context.params.mapsId)
      //   && myRoutes[context.params.gymId][context.params.mapsId].hasOwnProperty('length')){
      //       //update myRoutes with new information
      //
      //       routesList = myRoutes[context.params.gymId][context.params.mapsId];
      //       routesList.unshift(routeData.pushKey);
      //       return myRoutesDoc.update({[context.params.gymId+'.'+context.params.mapsId]: routesList} );
      //
      //   }else{//the doc exists, but the field does not exist for this gym and this mapId
      //     routesList = [routeData.pushKey];
      //     return myRoutesDoc.set({[context.params.gymId]: { [context.params.mapsId]: routesList}}, {merge:true});
      //   }
      // }else{//the myRoutes doc does not yet exist
      //     routesList = [routeData.pushKey];
      //     return myRoutesDoc.set({[context.params.gymId]: { [context.params.mapsId]: routesList}}, {merge:true});
      // }

    }else{//the route has no authorId. firestore.rules prevent it from being written
      return null;
    }
  }//not sure how this conditions ( snap.exists() ) ever fails here
  return null;
}

/*  onDeleteRouteDetails responsibilities:
  [*]delete details doc's parent route
  [*]delete all images from storage bucket for this route
  TODO: this code is not very robust. No try catch. No retry logic for various promises
  For example, if the returned Promise.all fails for any image delete promise, the remaining
  images will not be deleted. This should be improved.
*/
// async function onDeleteRouteDetails(snap:functions.firestore.DocumentSnapshot,
// context:functions.EventContext){
//   if(snap.exists){
//     const detailsDoc = snap.data();
//     console.log(`**##** detailsDoc :${ JSON.stringify(detailsDoc) } `);
//     // const photoPaths = detailsDoc.topo.photos;
//
//     //get a reference to the route doc that is the parent of this details doc
//     const routeDoc = admin.firestore().doc(`GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}`);
//     console.log(`routeDoc is: ${ JSON.stringify(routeDoc) } `);
//
//     //collect some info from this details doc's parent doc
//     const routeDataSnap = await routeDoc.get();
//     const routeData= await routeDataSnap.data();
//     const photoPaths = routeData.topo.photos;
//
//     //delete this details's doc's parent route doc
//     routeDoc.delete();
//
//     const storage = admin.storage();
//     const bucket = storage.bucket(bucketPath); //path imported from ./constants.ts
//
//     //Delete all images referenced by the deleted route's topo.
//     return Promise.all( photoPaths.map( path => bucket.file(path).delete() ));
//
//   }else{
//     throw new Error('Snapshot did not exist.');
//   }
//
// }

/*
responsibilities:
[-]delete subcollections
  [*]DETAILS
  []ANALYTICS
[*]delete route's photos (from storage bucket)
[]delete "myRoutes" entry for this route ( located at: USERS/{uid}/LISTS/myRoutes)

*/
async function onDeleteRoute(snap:functions.firestore.DocumentSnapshot,
context:functions.EventContext){
  if(snap.exists){

    //snap.data returns document data from right BEFORE the delete
    const deletedRouteData = snap.data();

    //NOTE: coverPhoto field should be a copy of one of these photos. If this changes, update this function to delete it!
    const photoPaths = deletedRouteData.topo.photos;
    // const authorId = deletedRouteData.authorId;

    /**********  NOTE: delete all SUBCOLLECTIONS here! ***********/
    //TODO: phasing out AUTHORS subcollection... remove this code after the last routes with AUTHORS/authorsDoc are deleted
    const authorsCollectionRef = admin.firestore()
    .collection(`GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}/AUTHORS/`);

    const authorsSnaps = await authorsCollectionRef.get();
    if (!authorsSnaps.empty){
      for(let authorSnap of authorsSnaps.docs){
        await authorSnap.ref.delete();
      }
    }

    /* Delete DETAILS */
    //get this route's DETAILS doc
    const detailsDoc = admin.firestore()
          .doc(`GYMS/${context.params.gymId}/MAPS/${context.params.mapsId}/ROUTES/${context.params.routeId}/DETAILS/detailsDoc`);

    //delete this route's DETAILS doc (and subcollection)
    detailsDoc.delete();

    /* Delete LISTS/myRoutes entry for this route's author */
    // try{ //try to delete this route from uid's myRoutes document
    //   const myRoutesDoc = admin.firestore()
    //     .doc(`USERS/${authorId}/LISTS/myRoutes`);
    //   const myRoutesSnap = await myRoutesDoc.get();
    //   //if the user in the AUTHORS document has a 'myRoutes' document...
    //   if(myRoutesSnap.exists){
    //     const myRoutesData = myRoutesSnap.data();
    //     //try to remove the routeId from their myRoutes document and resave it on the db
    //     if(myRoutesData.hasOwnProperty(context.params.gymId)// check for {gymId: { mapsId: { ...} } }
    //     && myRoutesData[context.params.gymId].hasOwnProperty(context.params.mapsId)){
    //       const routeIds = myRoutesData[context.params.gymId][context.params.mapsId] as Array<string>;
    //       const deleteIndex = routeIds.findIndex(routeId=>{
    //         return routeId === deletedRouteData.pushKey;
    //       });
    //       if(deleteIndex !== -1){
    //         routeIds.splice(deleteIndex,1); //remove the routeId that was deleted
    //         //build update object
    //         const myRoutesUpdate = {};
    //         myRoutesUpdate[`${context.params.gymId}.${context.params.mapsId}`] = routeIds;
    //         myRoutesDoc.update(myRoutesUpdate);
    //       }
    //     }
    //   }
    // }catch(error){
    //   console.log(error);
    // }

    /*Delete all COMMENTS and REPLIES docs*/
    await deleteAllCommentsAndReplies(context.params.gymId, context.params.mapsId,context.params.routeId)
    .catch(error => console.log(error)); //don't let an error stop this function from doing other work

    //*** Delete the FEEDBACK documents ***//
      const feedbackPath = snap.ref.path + `/FEEDBACK/`;
      const feedbackCollection = admin.firestore().collection(feedbackPath);
      const feedbackDocs = await feedbackCollection.get();
      feedbackDocs.forEach(async docSnap=>{
        if(docSnap.exists){
          await docSnap.ref.delete();
        }
      });

    /*** Delete the ANALYTICS/analyticsDoc (NOT user analytics. just route analytics)*************/
      const analyticsPath = snap.ref.path + `/ANALYTICS/analyticsDoc`;
      const analyticsDoc = admin.firestore().doc(analyticsPath);

      //TODO: delet this .get stuff... just for printing/debugging
      const analyticsSnap = await analyticsDoc.get();

      console.log('analyticsDoc has data:');
      console.log(analyticsSnap.data());

      await analyticsDoc.delete().catch(error=>{
        console.log('routeEvents, onDeleteRoute: error trying to delete analyticsDoc');
        console.log(error)}
      );
    /************************************************************/

    /********* Delete each user's record of sending this route (if they sent)******/

    console.log('about to build routeToUserSendsDocPath');
    // const routeToUserSendsDocPath = snap.ref.path+'/SENDS/sendsDoc';

    const routeToUserSendsDocPath = getRouteToUserSendsPath(context.params.gymId, context.params.routeId);
    console.log('routeToUserSendsDocPath:');
    console.log(routeToUserSendsDocPath);

    const sendsDoc = admin.firestore().doc(routeToUserSendsDocPath);
    const sendsSnap = await sendsDoc.get();

    // const sendsData = await getSendsData(context.params.gymId, context.params.routeId);

    if(sendsSnap.exists){
      const sendsData = sendsSnap.data();
      console.log('onDeleteRoute, sendsSnap exists! ');
      console.log('onDeleteRoute, got usersToSentObject as:');
      console.log(sendsData);

      Object.keys(sendsData).forEach(uid=>{
        console.log('trying to delete send for user ', uid);

        const userSendPath =`USERS/${uid}/SENDSBYGYM/${context.params.gymId}/SENDSBYMAPS/${context.params.mapsId}/SENDSBYROUTE/${context.params.routeId}`;
        const sendDoc = admin.firestore().doc(userSendPath);

        sendDoc.delete().catch(()=>{
          console.log('onDeleteRoute: error trying to delete USER send.')
        });
      });

      for(let uid of Object.keys(sendsData)){
        console.log('trying to delete send for user ', uid);
        const userSendPath =`USERS/${uid}/SENDSBYGYM/${context.params.gymId}/SENDSBYMAPS/${context.params.mapsId}/SENDSBYROUTE/${context.params.routeId}`;
        const sendDoc = admin.firestore().doc(userSendPath);
        await sendDoc.delete().catch(error =>{
          console.log('onDeleteRoute: error trying to delete USER send:');
          console.log(error);
        });
      }
      await sendsSnap.ref.delete();

      //delete the route-to-userId document for this route
    }

    //get the list of all user id's of ppl who sent this route
    //iterate through users and try to delete their record

    /********* Delete this Route's Photos from Storage Bucket **************/

    const storage = admin.storage();
    const bucket = storage.bucket(bucketPath); //path imported from ./constants.ts

    //Delete all images referenced by the deleted route's topo.
    return Promise.all( photoPaths.map( path => bucket.file(path).delete() )).catch(error=>{
      console.log(error);
      throw error;
    });
  }else{
    throw new Error('Could not delete route. snap.exists: false');
  }
}

async function getSendsData(gymId:string, routeId:string):Promise<{[keys:string]:any}>{
  const SENDSpath = getRouteToUserSendsPath(gymId,routeId);
  const sendsColRef = admin.firestore().doc(SENDSpath);
  const sendsSnap = await sendsColRef.get(); //there should only be one
  //if this route has sends...
  if(sendsSnap.exists){
    const sendsData = sendsSnap.data();
    return sendsData;
  }else{
    return null;
  }
}




//Might still use this for something else
//Get path for this image's parent directory (does not include bucket prefix)
// function pathFromPathWithFile(path:string){
//   return path.slice(0, path.lastIndexOf('/'));
// }

export const routeFunctions = {
  //onCreate: onCreate,
  // onDelete: onDeleteRouteDetails,
  onCreate: onCreateRoute,
  onDeleteRoute: onDeleteRoute

};
